# FIL

A single file libary for Fortran Image Libary.

This module is partially adapted from [WriteGIF](https://fortranwiki.org/fortran/show/writegif).


## Requirements
* [PorPre](https://gitlab.com/RomainNoel/porpre) a portable precision library
* [fLogger](https://gitlab.com/RomainNoel/flogger) a Fortran log library
* [FoST](https://gitlab.com/RomainNoel/fost) a
