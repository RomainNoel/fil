#!/usr/bin/env python3

# -*- coding: utf-8 -*-

"""
	NAME :   read_py_image.py


 DESCRIPTION :
	This script is made for reading a image and write the color matrix of this image into a binary file readable by fortran

 REQUIRED : in order to execute correctly this script you should have:
	- Droits utilisateur: users
	- avoir installé et ajouté au /bin ou au PATH (ou linké)les commandes
		- pdf(la)tex
		- ...
	- avoir un lecteur PDF installé pour utilisé l'option -l

 INPUT :
	First argument must be ...


 OUTPUT :
 	Files :
		- temporary files possibly cleaned with the option -R
		- File.out the output file resulting from the computation
		- File.log the log file
	Messages :
		-stdout (sortie 1) la réussite ou non des compilation intermédiaire
		-stderr (sortie 2) l'ensemble des erreurs et warning de l'opération

 ---------------------------------------------------------------------------------------
 USEFULL EXEMPLES:
 	Pour faire ....
 $>	./Prog.py --compil=xe --format=la --schema=CBINGc input.tex -R -r okular
 should give:
      input.pdf
 ---------------------------------------------------------------------------------------
 LICENCE :
 Copyright (C) 2017 Romain NOEL

	This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public
 License along with this program; if not, write to the Free
 Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA  02110-1301, USA.

 AUTHOR(S) :
 	Romain NOËL
		Mail	: romainnoel@laposte.net
		Adresse	: Ecole des Mines de Saint-Etienne, 42000 SAINT ETIENNE, FRANCE
"""

# Import of Libraries---------------------------------------------------------------------------------------------------
import os 					# librairy for system manipulation; sys is a submodule of os
import sys
import logging				# lib to manipulate loggers
import inspect				# lib to have access to the caller of this module

from PIL import Image
import numpy

try:
	import argcomplete		# lib for autocompletion of arguments
except:
	pass

# Initialisation of primary variables-----------------------------------------------------------------------------------
#
Author_name="Romain NOËL"
version_num=1.5
version_date="07-March-2019"
Prog_name="Read Image with Python"

(frame, filename, line_number, function_name, lines, index) = inspect.getouterframes(inspect.currentframe())[-1]
# print(frame, filename, line_number, function_name, lines, index)
print('caller is :', filename, 'and __name__ is :', __name__)
if filename.startswith('<string>') or filename.startswith('<stdin>') :
	filename_caller='my_utils'
else:
	# print('filename='+filename, flush=True)
	try:
		filename=filename.replace('\\','/')
		filename_caller= filename.rsplit('/',1)[1].rsplit('.py',1)[0]
	except IndexError as e:
		filename_caller= filename


logger = logging.getLogger(filename_caller+'.my_utils')


################################ 	FONCTIONS & VARIABLES 	########################################


### Extracted from my_init_scripts.py version 1.01
def init_logs_and_files(pargs, logging, logfilename):
	""" """

	# Initialisation of files  -------------------------------------------------
	#exec 3>fichier
	#exec 4<fichier

	# ensure the encoding of the standard outputs
	sys.stdout = open(sys.stdout.fileno(), mode='w', encoding='utf8')
	sys.stderr = open(sys.stderr.fileno(), mode='w', encoding='utf8')
	# sys.stdout = codecs.getwriter('utf8')(sys.stdout)

	# Definition of the log level redirections
	f0 = sys.stdout				# Emergency
	f1 = sys.stdout				# Alert
	f2 = sys.stdout 			# Critical
	f3 = sys.stdout				# Error
	f4 = open(os.devnull,"w")	# Warning
	f5 = open(os.devnull,"w")	# Notice
	f6 = open(os.devnull,"w")	# Info
	f7 = open(os.devnull,"w")	# Debug
	f8 = open(os.devnull,"w")	# Trace

	#  Creating the custumed log levels
	logging.addLevelName(75, "EMERGENCY")
	def emergency(self, message, *args, **kws):
		# Yes, logger takes its '*args' as 'args'.
		if self.isEnabledFor(75):
			self._log(75, message, args, **kws)
	logging.Logger.emergency = emergency
	logging.EMERGENCY = 75

	logging.addLevelName(55, "ALERT")
	def alert(self, message, *args, **kws):
		# Yes, logger takes its '*args' as 'args'.
		if self.isEnabledFor(55):
			self._log(55, message, args, **kws)
	logging.Logger.alert = alert
	logging.ALERT = 55

	logging.addLevelName(25, "Notice")
	def notice(self, message, *args, **kws):
		# Yes, logger takes its '*args' as 'args'.
		if self.isEnabledFor(25):
			self._log(25, message, args, **kws)
	logging.Logger.notice = notice
	logging.NOTICE = 25

	logging.addLevelName(5, "Trace")
	def trace(self, message, *args, **kws):
		# Yes, logger takes its '*args' as 'args'.
		if self.isEnabledFor(5):
			self._log(5, message, args, **kws)
	logging.Logger.trace = trace
	logging.TRACE = 5


	# create logger with based on the logfilename
	logger = logging.getLogger(logfilename)

	# create file handler
	fh = logging.FileHandler(logfilename+'.log', mode='w') # by default is mode='a'
	# fh = logging.handlers.RotatingFileHandler('mptest.log', 'a', 300, 10)

	# create console handler
	ch = logging.StreamHandler(stream=sys.stdout)

	# create formatter and add it to the handlers
	formatter_fh = logging.Formatter('%(asctime)s; %(process)d - %(thread)d; %(name)s - [%(levelname)s] : %(message)s (%(funcName)s)')
	formatter_ch = logging.Formatter('[%(levelname)s] : %(message)s (%(name)s - %(funcName)s)' ) # , datefmt='%m/%d/%Y %I:%M:%S'
		# %(asctime)s 	Human-readable time
		# %(pathname)s 	Full pathname of the source file where the logging call was issued (if available).
		# %(name)s 	Name of the logger used to log the call.
		# %(message)s 	The logged message
		# %(levelname)s 	Text logging level for the message ('DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL')
		# %(funcName)s 	Name of function containing the logging call.
		# %(process)d 	Process ID (if available).
		# %(processName)s 	Process name (if available).
		# %(thread)d 	Thread ID (if available).
		# %(threadName)s 	Thread name (if available).

	fh.setFormatter(formatter_fh)
	ch.setFormatter(formatter_ch)

	# set log level by default
	logger.setLevel(logging.ERROR)
	# fh.setLevel(logging.DEBUG)
	# ch.setLevel(logging.TRACE)

	# Parametering logging and redirection according to the given arguments
	if pargs.loglevel >= 4 :
		f4=sys.stdout
		logger.setLevel(logging.WARNING)
		# fh.setLevel(logging.WARNING)
		# ch.setLevel(logging.WARNING)
	if pargs.loglevel >= 5 :
		f5=sys.stdout
		logger.setLevel(logging.NOTICE)
		# fh.setLevel(logging.NOTICE)
		# ch.setLevel(logging.NOTICE)
	if pargs.loglevel >= 6 :
		f6=sys.stdout
		logger.setLevel(logging.INFO)
		# fh.setLevel(logging.INFO)
		# ch.setLevel(logging.INFO)
	if pargs.loglevel >= 7 :
		f7=sys.stdout
		logger.setLevel(logging.DEBUG)
		# print('logger7=',logger.getEffectiveLevel())
		# fh.setLevel(logging.DEBUG)
		# ch.setLevel(logging.DEBUG)
	if pargs.loglevel >= 8 :
		f8=sys.stdout
		logger.setLevel(logging.TRACE)
		# fh.setLevel(logging.TRACE)
		# ch.setLevel(logging.TRACE)


	# add the handlers to the logger
	logger.addHandler(fh)
	logger.addHandler(ch)

	# Add explanations at beginning of the log file
	# TODO

	return f0, f1, f2, f3, f4, f5, f6, f7, f8, logger

def run_program():
	''' fonction bidon'''
	print('.', end='', file=f7)





################################ 	MAIN SCRIPT 	########################################


#
if __name__ == '__main__':
	# # Import of Libraries-------------------------------------------------------
	# import my_init_scripts
	# import my_utils

	# # Initialisation of primary variables---------------------------------------
	# #
	# list_proc, main_PID = my_init_scripts.init_attributes_required_signal()
	main_PID=os.getpid()

	# Read the options----------------------------------------------------------
	def init_args():
		import argparse
		try:
			import argcomplete		# lib for autocompletion of arguments
		except:
			pass

		parser = argparse.ArgumentParser(prefix_chars='-+', add_help=True, formatter_class=argparse.RawTextHelpFormatter,
			description='Program Description in the help message')

		parser.add_argument('-V', '--version' ,action="version", dest='version',
			version="[OK] \n \n File: %(prog)s\n Program's name: {} \n Version: {} \n Date: {}".format(Prog_name, version_num, version_date) ,
			help="show the program's version number and exit")

		# argument non optional (without -)
		#parser.add_argument('comp_arg', action="store" ,help="show")
		#requiredNamed = parser.add_argument_group('required named arguments')
		#requiredNamed.add_argument('-i', '--input', help='Input file name', required=True)


		#parser.add_argument('-short', '--longa', action="store", default="toto", dest="a", nargs=3, type=int(eger), help='something to know about this option')
			#action="store_true"	: store a boolean flag True or false
			#action="store"			: store the value in the argument specification
			#action="store_const"	: store a non-boolean flag
			#action="append"		: add to the argument specification in the list
			#action="append_const"	: add alway the same flag in the list
			#action="count"			: add alway the same flag in the list
			#nargs=N				: the option requires exactly N arguments
			#nargs='?				: the option requires 0 or 1 arguments
			#nargs='*'				: the option requires 0 or all the arguments
			#nargs='+'				: the option requires 1 or all arguments

		parser.add_argument('-i', '--input', help='Input image file name', dest='inputfile', action="store")
		parser.add_argument('-p', '--param', help='Input parameters file name', dest='inputparam', action="store")
		parser.add_argument('-o', '--output', help='Output file name', dest='output', action="store")

		# groupe exclusive
		group_excluV = parser.add_mutually_exclusive_group()
		group_excluV.add_argument('-v', '--verbose', action="count", default=4, dest='loglevel', help='increase the program verbosity')
		group_excluV.add_argument('-q', '--quiet', action="store_const", const=3, dest='loglevel', help='make this program runs quietly')
		group_excluV.add_argument('-d', '--debug', action="store_const", dest="loglevel", const=8,
			help="Print lots of debugging statements")

		# subparser
		#subparsers=parser.add_subparsers()
		#parser_foo = subparsers.add_parser('foo')


		if 'argcomplete' in sys.modules:
			argcomplete.autocomplete(parser)

		return parser

	parser = init_args() # init args
	args = parser.parse_args() # read args
	# args = parser.parse_args(['-vvv']) # to specify args when short execution
	# print('[WARNING] !!! The given arguments are hard coded')
	# print ( args ) # print the list of all the argument with their status

	# Setting Path variables and import conf file
	# os.chdir('../')
	# chroot_path=os.getcwd()
	# os.chdir('./bin')
	# var_env=dict(os.environ)
	# conf=import_conf_file()

	# Initialisation of files, logs and redirections ---------------------------
	logfilename=os.path.basename(__file__).rsplit('.py',1)[0]
	f0, f1, f2, f3, f4, f5, f6, f7, f8, logger= init_logs_and_files(args, logging, logfilename)

	############
	### MAIN ###
	############
	# Beginnging of the main script
	print("[Notice] Main read_py_image sequence: Start (PID={}) \n".format(main_PID), file=f5 )
	logger.notice("Main read_py_image sequence Script: Start (PID={})".format(main_PID) )#, file=f5, flush= True )
	####


	input_file=args.inputfile #'Karman1.png' #'path3336.png' 'test2.gif'
	input_param=args.inputparam
	output_file=args.output #'image_RGB.bin'

	try:
		# Reading colors parameter
		print("[Notice] Try to read the input parameters", file=f5)
		f=open(input_param)#source_dir+input_param)
		lines=f.readlines()
		j=0
		start=0
		end=0
		for index,line in enumerate(lines):
			if "<color_index_image" in line:
				start= index
			elif "</color_index_image" in line:
				end=index
		if start==0 or end==0:
			print("[ERROR] the color_index_image tag is not found in the input xml file.", file=f3)
			sys.exit(6)
		clr_map= numpy.empty( (end-start,5 ), dtype='f8')
		# print(lines[start+1:end])
		for index,line in enumerate(lines[start+1:end]):
			if "<color_index" in line:
				# I get the index color
				# print( line.split('index="')[1].split('"')[0] )
				clr_map[j,0]= int( line.split('index="')[1].rsplit('"')[0] )
				# Then the RGB code
				buffer=line.split('RGBN="')[1].rsplit('"')[0]
				start = buffer.index( '(' ) + len( '(' )
				end = buffer.index(')') #
				li=list(map(float,buffer[start:end].split(",") ) ) # I parse it into a list
				li.append(1.0) # for the transparency
				# print(li)
				clr_map[j,1:5] = numpy.array( li )
				j=j+1 	
			else:
				print("[ERROR] Unknown tag in the color_index_image:"+line, file=f3)
				sys.exit(7)

		# Reading the image
		print("[Notice] Try to read the input image", file=f5)
		#image_input= misc.imread(source_dir+input_file, mode='RGBA')
		image_input=Image.open(input_file)#source_dir+input_file) # Read the header of the file with PIL
		print('[Trace]',type(image_input),'image size:', image_input.size, file=f8)

		RGBA_NorMa=numpy.array(image_input.convert(mode='RGBA'), dtype='f8' ) /255
		Im_CI= numpy.empty((RGBA_NorMa.shape[0],RGBA_NorMa.shape[1]), dtype='i4')
		for i in range(RGBA_NorMa.shape[0]):
			for j in range(RGBA_NorMa.shape[1]):
				 for k in range(clr_map.shape[0]):
				 	#print('k={}/{}'.format(k,clr_map.shape[0]))
				 	if (RGBA_NorMa[i,j,0:3] == clr_map[k,1:4]).all():
				 		Im_CI[i,j]=clr_map[k,0]#100+k
				 		# clr_map[k,0]=Im_CI[i,j]
				 		break
				 	else:
				 		if k==clr_map.shape[0]-1:
				 			print('[WARN] the RGB color {} at {}, {} is not in the clr_map'.format(RGBA_NorMa[i,j,0:3],i,j), file=f4)
				 			l=0
				 			print('[WARN] the RGB color {} at {}, {} is transfert to {}'.format( RGBA_NorMa[i,j,0:3], i, j, clr_map[l,1:4] ), file=f4)
				 			Im_CI[i,j]=100+l
				 			clr_map[l,0]=Im_CI[i,j]
				 			#sys.exit(5)


		#print(RGBA_NorMa[50,:], file=f8)
		#print(Im_CI[50,:], file=f8)
		print("[Trace] color_map (indice, R,G,B,A):\n", clr_map, file=f8)

		# writing into a binary file the RGB matrix associated to the image
		print('[Notice] Writing the Color_indexed and Color_map matrix', file=f5)
		with open(output_file,'wb') as f:   							 # destination_dir+output_file
			print('[Trace]','image Color_indexed type={}'.format(type(Im_CI[0,0])),file=f8)
			f.write( numpy.array([Im_CI.shape[0],Im_CI.shape[1]], dtype='i4'))
			f.write( numpy.array(Im_CI, dtype='i2') )
			# print( numpy.array(Im_CI, dtype='i2') )
			print('[Trace]','Color_map type={}'.format(type(clr_map[0,0])),file=f8)
			f.write( numpy.array([clr_map.shape[0],clr_map.shape[1]-1], dtype='i4') )
			# print( numpy.array([clr_map.shape[0],clr_map.shape[1]-1], dtype='i4') )
			f.write( numpy.array(clr_map[:,0:4], dtype='f4') )
			# print( numpy.array(clr_map[:,0:4]) )
			f.close()
		print("[Notice] Writing end", file=f5)


	#except Exception as e:
	except :
		print ("Unexpected exception:", sys.exc_info() )
		print ("[ALERT] Somethin went wrong while reading the input image with read_py_image: STOP", file=f3)

		raise
		print ('whatever I WANTTT')
		sys.exit(5)
	else:
		# Do this only if the try successed
		pass
	finally:
		# Do this after the try + else or the exception
		pass


	# Print the normal end
	print("[Notice] End of read_py_image Script: 0 **\n", file=f5 )

	sys.exit(0)


######################## 	TO DO 	     	########################
'''
toto
'''

######################## 	BUGS KNOWN     	########################
'''
toto
'''

######################## 	AIDE MEMOIRE 	########################
'''
toto
'''

#EOF
